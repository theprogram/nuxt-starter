export default {
  target: 'static',
  head: {
    title: 'nuxt-ts',
    htmlAttrs: { lang: 'en' },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description', name: 'description', content: '',
      },
    ],
    link: [{
      rel: 'icon', type: 'image/x-icon', href: '/favicon.ico',
    }],
  },
  plugins: [],
  components: true,
  buildModules: ['@nuxt/typescript-build', '@nuxtjs/tailwindcss'],
  modules: [],
  content: {},
  build: {},
  env: {
    TOKEN: process.env.TOKEN,
    SPACE: process.env.SPACE,
  },
  css: ['~/assets/styles/base'],
  loading: false,
  tailwindcss: {
    jit: true,
    exposeConfig: true,
  },
  publicRuntimeConfig: {
    author: 'The Program',
    cdn: '',
  },
};
