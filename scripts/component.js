'use strict';

import fs from 'fs/promises';

(async () => {
  const [
    ,, arg,
  ] = process.argv;

  if (!arg) {
    throw new Error('\n\n\nYou must supply a component name.\n\n');
  }

  const dir = `./components/${arg}`;
  const component = `<template>\n  <div>${arg} Component</div>\n</template>\n\n\n<script lang="ts">\nimport Vue from 'vue';\n\nexport default Vue.extend({\n  name: '${arg}',\n  props: {},\n});\n</script>\n\n\n<style src="./${arg}.scss" lang="scss" scoped></style>\n`;
  const types = 'export interface ObjectProp {}\n';
  const styles = 'div {\n  contain: content;\n}\n';
  const test = `import { render, screen } from '@testing-library/vue';\nimport ${arg} from './${arg}.vue';\n\ndescribe('${arg}', () => {\n  test('renders its content', () => {\n    render(${arg});\n\n    const text = screen.getByText(/${arg} Component/);\n\n    expect(text).toBeInTheDocument();\n  });\n});\n`;

  try {
    await fs.mkdir(dir);
  } catch {}

  await fs.writeFile(`${dir}/types.d.ts`, types);
  await fs.writeFile(`${dir}/${arg}.scss`, styles);
  await fs.writeFile(`${dir}/${arg}.spec.ts`, test);
  await fs.writeFile(`${dir}/${arg}.vue`, component);

  process.stdout.write(`\nComponent ${arg} created.\n\n`);
})();

