'use strict';

import fss, { promises as fs } from 'fs';

(async () => {
  const [
    ,, arg,
  ] = process.argv;

  if (!arg) {
    throw new Error('\n\n\nYou must supply a page name.\n\n');
  }

  const dir = './pages';

  if (fss.existsSync(`${dir}/${arg}.vue`)) {
    process.stdout.write(`\n\n\nFile ${arg}.vue already exists.\n\n`);

    return;
  }

  const name = arg.toUpperCase().replace(/-/g, '');
  const page = `<template>\n  <Main>\n    <!-- Content -->\n  </Main>\n</template>\n\n\n<script lang="ts">\nimport Vue from 'vue';\nimport request from '@/plugins/request';\nimport { Page } from '@/typescript/contentful';\nimport { Entry } from '@/constants';\n\nexport default Vue.extend({\n  async asyncData() {\n    const { page } = await request<Page>(\`#graphql\n      {\n        page(id: "\${Entry.${name}}") {\n          title\n          description\n        }\n      }\n    \`);\n\n    return { page };\n  },\n  head() {\n    const { title, description } = this.$data.page || {};\n\n    return {\n      title,\n      meta: [{\n        hid: 'home-description',\n        name: 'description',\n        content: description,\n      }],\n    };\n  },\n});\n</script>\n`;

  await fs.writeFile(`${dir}/${arg}.vue`, page);

  process.stdout.write(`\n${arg}.vue created.\n\n`);
})();
