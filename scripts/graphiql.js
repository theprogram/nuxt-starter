'use strict';

import open from 'open';
import dotenv from 'dotenv';

(async () => {
  dotenv.config();

  const { SPACE, TOKEN } = process.env;

  process.stdout.write('\n\nOpening graphiql in your default browser...\n');
  await open(`https://graphql.contentful.com/content/v1/spaces/${SPACE}/explore?access_token=${TOKEN}`);
})();
