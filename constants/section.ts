export const sectionTypes = [
  'section', 
  'header', 
  'article',
] as const;

export type SectionType = typeof sectionTypes[number];
