import { GraphQLClient, gql } from 'graphql-request';

const { SPACE } = process.env;
const { TOKEN } = process.env;

const request = async <Q>(query: string): Promise<Q> => {
  const client = new GraphQLClient(
    `https://graphql.contentful.com/content/v1/spaces/${SPACE}`,
    {
      headers: {
        'Content-Type': 'application/json',
        'authorization': `Bearer ${TOKEN}`,
      },
    },
  );

  const data = await client.request(gql`${query}`);

  return data;
};

export default request;
// export { gql };
