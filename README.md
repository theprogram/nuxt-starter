# The Program Nuxt Starter

This project uses

- [Yarn 2](https://yarnpkg.com/getting-started/migration)
- [Node 14](https://nodejs.org/en/) or higher
- [Typescript](https://www.typescriptlang.org/)

## Build Setup

Install packages

```bash
yarn
```

Start the project on a local server

```bash
yarn start
```

Build for production and launch server
```bash
yarn build

yarn serve
```

Generate static HTML build
```bash
yarn generate
```

## Dev Commands

Create a new component

```bash
yarn component <ComponentName>
```

Create a new page
```bash
yarn page <page-name>
```

Launch GraphiQL in your browser

```bash
yarn graphiql
```

## Recommended Extensions for VS Code

- ESLint from Dirk Baeumer
- Vetur from Pine Wu
- GraphQL from GraphQL Foundation
