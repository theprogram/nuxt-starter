type _string = Maybe<string>;
type _number = Maybe<number>;
type R<T> = Readonly<T>;
type RA<T> = ReadonlyArray<T>;

/**
 * Contentful Types
 */

export type ID = R<{
  sys: R<{
    id: string;
  }>;
}>;

export type Asset = R<{
  title: _string;
  url: _string;
  description: _string;
  contentType: _string;
}>;


/**
 * Model Types
 */

export type Image = ID & R<{
  desktop: R<{
    description: _string;
    url: _string;
  }>;
  mobile: R<{
    url: _string;
  }>;
}>;

export type Content = ID & R<{
  heading: _string;
  copy: _string;
  asset: Maybe<Image>;
}>;


/**
 * Page Types
 */

export type Page = R<{
  page: R<{
    title: _string;
    description: _string;
    content: R<{
      items: RA<Content>;
    }>;
  }>
}>;

