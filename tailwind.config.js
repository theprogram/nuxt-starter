module.exports = {
  purge: [
    './assets/**/*.scss',
    './components/**/*.vue',
    './layouts/**/*.vue',
    './pages/**/*.vue',
  ],
  corePlugins: [],
  darkMode: false,
  theme: {
    colors: {
      'white': 'var(--white)',
      'black': 'var(--black)',
      'primary': 'var(--primary)',
      'secondary': 'var(--secondary)',
      'warning': 'var(--warning)',
      'info': 'var(--info)',
      'error': 'var(--error)',
      'success': 'var(--success)',
      'text': {
        'primary': 'var(--text-primary)',
        'secondary': 'var(--text-secondary)',
      },
      'surface': {
        'primary': {
          1: 'var(--surface-primary-1)',
          2: 'var(--surface-primary-2)',
          3: 'var(--surface-primary-3)',
        },
        'secondary': {
          1: 'var(--surface-secondary-1)',
          2: 'var(--surface-secondary-2)',
        },
        'accent': { 1: 'var(--surface-accent-1)' },
      },
      'grey': {
        1: 'var(--grey-1)',
        2: 'var(--grey-2)',
        3: 'var(--grey-3)',
        4: 'var(--grey-4)',
        5: 'var(--grey-5)',
        6: 'var(--grey-6)',
        7: 'var(--grey-7)',
        8: 'var(--grey-8)',
        9: 'var(--grey-9)',
      },
    },
    fontFamily: {
      sans: [
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
      ],
      serif: ['Georgia', 'serif'],
      mono: ['Menlo', 'monospace'],
    },
    spacing: {
      1: '9px',
      2: '15px',
      3: '30px',
      4: '60px',
      5: '90px',
      6: '120px',
    },
  },
};
