import { render, screen } from '@testing-library/vue';
import Footer from './Footer.vue';


const author = 'The Program';

describe('Footer', () => {
  test('renders its content', () => {
    render(Footer, { mocks: { $config: { author } } });

    const copyright = screen.getByText(/Copyright 2021 The Program/);

    expect(copyright).toBeInTheDocument();
  });
});
