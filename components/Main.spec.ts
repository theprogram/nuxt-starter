import { render, screen } from '@testing-library/vue';
import Main from './Main.vue';


describe('Main', () => {
  test('renders its content', () => {
    render(Main);

    const main = screen.getByRole('main');

    expect(main).toBeInTheDocument();
  });
});
