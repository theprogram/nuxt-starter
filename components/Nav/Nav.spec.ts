import { render, screen } from '@testing-library/vue';
import { RouterLinkStub } from '@vue/test-utils';
import { Nav as Links } from '@/constants';
import Nav from './Nav.vue';


const [
  Home,
  About,
  Contact,
] = Object.keys(Links);

describe('Nav', () => {
  test('renders its content', () => {
    render(Nav, {
      mocks: { $route: { path: '/' } },
      stubs: { NuxtLink: RouterLinkStub },
    });

    const nav = screen.getByRole('navigation', { name: /Site Navigation/ });
    const home = screen.getByRole('link', { name: Home });
    const about = screen.getByRole('link', { name: About });
    const contact = screen.getByRole('link', { name: Contact });

    expect(nav).toBeInTheDocument();
    expect(home).toBeInTheDocument();
    expect(home).toHaveAttribute('href', Links.Home);
    expect(home).toHaveAttribute('aria-current', 'page');
    expect(about).toBeInTheDocument();
    expect(about).toHaveAttribute('href', Links.About);
    expect(about).not.toHaveAttribute('aria-current');
    expect(contact).toBeInTheDocument();
    expect(contact).toHaveAttribute('href', Links.Contact);
    expect(contact).not.toHaveAttribute('aria-current');
  });
});
