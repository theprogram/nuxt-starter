import { render, screen } from '@testing-library/vue';
import ResImage from '@/components/ResImage/ResImage.vue';
import Section from './Section.vue';
import SectionComponent from './Component.vue';


describe('Section', () => {
  test('renders its content', () => {
    render(Section, {
      props: {
        heading: 'Hello Section',
        copy: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        asset: {
          desktop: {
            description: '',
            url: '/foo.jpg',
          },
          mobile: { url: '/bar.jpg' },
        },
      },
      components: { ResImage, SectionComponent },
    });

    const header = screen.getByRole('region', { name: /Hello Section/ });
    const heading = screen.getByRole('heading', { name: /Hello Section/ });
    const copy = screen.getByText(/Lorem ipsum dolor sit amet/);
    const image = screen.getByRole('img');

    expect(header).toBeInTheDocument();
    expect(heading).toBeInTheDocument();
    expect(copy).toBeInTheDocument();
    expect(image).toBeInTheDocument();
    expect(image).toHaveAttribute('src', '/foo.jpg');
  });
});
