import { render, screen } from '@testing-library/vue';
import ResImage from './ResImage.vue';

describe('ResImage', () => {
  test('won\'t render without desktop', () => {
    const { container } = render(ResImage);

    expect(container).toBeEmptyDOMElement();
  });

  test('renders its content', () => {
    render(ResImage, {
      props: {
        desktop: '/foo.jpg',
        mobile: '/bar.jpg',
        text: 'Foo Bar',
      },
    });

    const img = screen.getByRole('img', { name: /Foo Bar/ });

    expect(img).toBeInTheDocument();
    expect(img).toHaveAttribute('src', '/foo.jpg');
  });
});
